import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from '../interfaces/pokemon.interface';
import { PokemonService } from '../services/pokemon.service';

@Pipe({
  name: 'pagination'
})
export class PaginationPipe implements PipeTransform {

  constructor(private pokemonService: PokemonService) {

  }
  transform (pokemons: Pokemon[], page: number = 0, term: string = ""): Pokemon[] {

    if (term.length === 0) {
      this.pokemonService.setTotalOfPokemons(pokemons.length);
      return pokemons.slice(page, page + 5);
    }

    const filteredPokemons: Pokemon[] = pokemons
      .filter(pokemons => pokemons.name.includes(term))

    this.pokemonService.setTotalOfPokemons(filteredPokemons.length);

    return filteredPokemons.slice(page, page + 5);;

  }

}
