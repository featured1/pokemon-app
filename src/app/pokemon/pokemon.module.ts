import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonListComponent } from './pokemon-list/pokemon-list.component';
import { PaginationPipe } from './pipes/pagination.pipe';



@NgModule({
  declarations: [
    PokemonListComponent,
    PaginationPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PokemonListComponent
  ]
})
export class PokemonModule { }
