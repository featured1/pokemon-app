import { debounce } from 'rxjs/operators';
import { BehaviorSubject, timer } from 'rxjs';
import { Pokemon } from '../interfaces/pokemon.interface';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  @ViewChild('txtSearch') txtSearchRef;

  public page: number = 0;
  public term: string = "";
  public pokemons: Pokemon[] = [];
  public totalOfPokemons: number = 0;
  private termSubject: BehaviorSubject<string> = new BehaviorSubject("");

  constructor(private pokemonService: PokemonService) { }

  ngOnInit (): void {

    this.pokemonService.getAllPokemons()
      .subscribe(pokemons => {
        this.pokemons = pokemons;
      });

    this.pokemonService.totalOfPokemons
      .subscribe((totalOfPokemons: number) => {
        this.totalOfPokemons = totalOfPokemons;
      });


    // Takes the value of term emitted the last 300ms and save it to the term property
    this.termSubject
      .pipe(debounce(() => timer(300)))
      .subscribe((term) => {
        this.term = term;
      });

  }

  ngOnDestroy (): void {
    this.termSubject.unsubscribe();
    this.pokemonService.totalOfPokemons.unsubscribe();
  }


  onPreviousPage (): void {
    if (this.page > 0)
      this.page -= 5;
  }

  onNextPage (): void {
    this.page += 5;
  }

  onSearchPokemon (term: string): void {
    this.page = 0;
    this.termSubject.next(term);

    if (term.length === 0) {
      this.txtSearchRef.nativeElement.value = "";
    }
  }
}
