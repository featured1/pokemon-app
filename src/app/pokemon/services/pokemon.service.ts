import { BehaviorSubject, Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FetchAllPokemonResponse, Pokemon } from '../interfaces/pokemon.interface';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private url: string = "https://pokeapi.co/api/v2";
  public totalOfPokemons: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor(private http: HttpClient) {

  }

  public getAllPokemons (): Observable<Pokemon[]> {
    return this.http.get<FetchAllPokemonResponse>(`${this.url}/pokemon?limit=1500`)
      .pipe(
        map(this.transformSmallPokemon)

      );
  }

  public setTotalOfPokemons (total: number): void {
    this.totalOfPokemons.next(total);
  }

  private transformSmallPokemon (response: FetchAllPokemonResponse): Pokemon[] {

    const pokemonList: Pokemon[] = response.results.map(pokemon => {

      const pokemonURISegments = pokemon.url.split("/");
      const pokemonId = pokemonURISegments[6];

      const picture = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/${pokemonId}.png`;

      return {
        id: pokemonId,
        picture,
        name: pokemon.name
      }

    });

    return pokemonList;

  }
}
